const path = require('path')
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/tstype-to-phpclass/'
module.exports = {
	env: { SITE_NAME: 'tstype to phpclass' },
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	webpack(config, _options) {
		config.resolve.alias = {
			...config.resolve.alias,
			'~': path.resolve(__dirname, 'src'),
		}
		config.node = {
			...config.node,
			fs: 'empty',
			// eslint-disable-next-line @typescript-eslint/camelcase
			child_process: 'empty',
		}
		return config
	},
}
