import React from 'react'
import NextApp from 'next/app'
import Head from 'next/head'

class App extends NextApp {
	render() {
		const { Component, pageProps } = this.props

		return (
			<>
				<Head>
					<title>{process.env.SITE_NAME}</title>
				</Head>
				<main>
					<Component {...pageProps} />
				</main>
			</>
		)
	}
}

export default App
