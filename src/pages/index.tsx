import React, { useState, useCallback } from 'react'
import { usePHPClass } from '~/lib/usePHPClass'

const cancelTab = (e: React.KeyboardEvent) => {
	if (e.nativeEvent.isComposing || 'Tab' !== e.key) return
	const elem = e.target
	if (!(elem instanceof HTMLTextAreaElement)) return
	const { selectionStart: start, selectionEnd: end, value: value } = elem
	elem.value = '' + value.substring(0, start) + '\t' + value.substring(end)
	elem.selectionStart = elem.selectionEnd = start + 1
	e.preventDefault()
	return false
}

const tmp = `interface Options {
	dryrun?: boolean
}`
export const App = () => {
	const [text, settext] = useState(tmp)
	const onChange = useCallback(
		(e: React.FormEvent) => {
			if (e.target instanceof HTMLTextAreaElement) {
				settext(e.target.value)
			}
		},
		[settext],
	)
	const php = usePHPClass(text)
	return (
		<div>
			<div>
				<textarea value={text} onChange={onChange} onKeyDown={cancelTab} />
			</div>
			<div>
				{php.isPending ? (
					<p>loading...</p>
				) : (
					<textarea value={php.value || ''} disabled={php.isPending} readOnly />
				)}
			</div>
			<style jsx>{`
				textarea,
				p {
					width: 80vw;
					height: 40vh;
					font-family: monospace, system-ui;
				}
				p {
					padding: 3px;
					font-size: 80%;
					background: #eee;
				}
			`}</style>
		</div>
	)
}

export default App
