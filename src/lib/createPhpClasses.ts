import * as ts from 'typescript'

export const createSource = (code: string, name: string) =>
	ts.createSourceFile(
		name,
		code,
		ts.ScriptTarget.ES2019,
		false,
		ts.ScriptKind.TS,
	)

export const createPhpClasses = (source: ts.SourceFile) => {
	const files: { name: string; lines: string[] }[] = []
	ts.forEachChild(source, node => {
		if (!ts.isInterfaceDeclaration(node)) return
		const nodename = String(node.name.escapedText)
		// if (!entityNames.includes(nodename)) return
		if (!node.members) return
		const lines: string[] = []
		const needCallSetters: string[] = []
		const builder = new PHPClassBuilder(
			line => lines.push(line),
			name => needCallSetters.push(name),
		)
		// lines.push('<?php')
		// lines.push('namespace Knock\\WPPlugins\\DPlusCore\\Entity;')
		// lines.push('')
		lines.push(`class ${nodename} {`)
		node.members.map(node => {
			if (!ts.isPropertySignature(node)) return
			const property = node.name.getText(source)
			if (!node.type) return console.log('empty node.type')
			const optional = !!node.questionToken
			builder.read(property, node.type.getText(source), optional)
		})
		lines.push('')
		lines.push(`	static function fromarray(array $r): self {`)
		lines.push(`		$self = new static;`)
		for (const name of needCallSetters)
			lines.push(`		$self->set_${name}($r['${name}'] ?? null);`)
		lines.push(`		return $self;`)
		lines.push(`	}`)
		lines.push(`}`)
		lines.push('')
		files.push({ name: nodename, lines })
	})
	return files
}

type ReaderFunction = (builder: PHPClassBuilder, propertyName: string) => void
export class PHPClassBuilder {
	constructor(
		private readonly _pushLine: (line: string) => void,
		private readonly _addPropName?: (name: string) => void,
		private readonly _readers: [string, ReaderFunction][] = [],
	) {}
	addtype(tsType: string, fn: ReaderFunction) {
		this._readers.push([tsType, fn])
		return this
	}
	// eslint-disable-next-line complexity
	read(propertyName: string, tsType: string, optional = false) {
		const name = propertyName
		this._pushLine(`	private $${name};`)
		const pre = optional ? '?' : ''
		switch (tsType) {
			case 'string':
				this.accessor(name, pre + 'string').both()
				return
			case 'string | null':
				this.accessor(name, '?string').both()
				return
			case 'number':
				this.accessor(name, pre + 'int').both()
				return
			case 'number | null':
				this.accessor(name, '?int').both()
				return
			case 'boolean':
				this.accessor(name, pre + 'bool').both()
				return
			case 'boolean | null':
				this.accessor(name, '?bool').both()
				return
			case "'male' | 'female' | null":
				this.accessor(name, '?string')
					.getter()
					.setter(`null === $v or 'male' === $v or 'female' === $v`)
				return
			case 'object':
				this.accessor(name, '').both()
				return
			case 'number[]':
				this.accessor(name, 'array')
					.getter()
					.setter(
						`\\call_user_func(function() use ($v) {`,
						`	foreach ($v as $p) if (!\\is_int($p)) return false;`,
						`	return true;`,
						`})`,
					)
				return
			case 'string[]':
				this.accessor(name, 'array')
					.getter()
					.setter(
						`\\call_user_func(function() use ($v) {`,
						`	foreach ($v as $p) if (!\\is_string($p)) return false;`,
						`	return true;`,
						`})`,
					)
				return
			default:
				for (const [t, fn] of this._readers) {
					if (t === tsType) {
						fn(this, t)
						return
					}
				}
				throw new Error('要対応: ' + tsType)
		}
	}
	accessor(name: string, type: string) {
		return new AccessorBuilder(name, type, this._pushLine, this._addPropName)
	}
}

export class AccessorBuilder {
	constructor(
		private readonly _name: string,
		private readonly _type: string,
		private readonly _pushLine: (line: string) => void,
		private readonly _addPropName?: (name: string) => void,
	) {}
	both() {
		return this.getter().setter()
	}
	getter() {
		const name = this._name
		const type = this._type ? `: ${this._type}` : ''
		this._pushLine(`	function ${name}()${type} { return $this->${name}; }`)
		return this
	}
	setter(...conds: string[]) {
		const name = this._name
		const type = this._type ? this._type + ' ' : this._type
		if (conds.length) {
			const cond =
				1 === conds.length
					? conds.join(';')
					: conds.map(c => '\n\t\t\t' + c).join('') + '\n\t\t'
			const stmt = `if (!(${cond})) throw new \\Error('type error');`
			this._pushLine(`	function set_${name}(${type}$v) {`)
			this._pushLine(`		${stmt}`)
			this._pushLine(`		$this->${name} = $v;`)
			this._pushLine(`	}`)
		} else {
			this._pushLine(`	function set_${name}(${type}$v) { $this->${name} = $v; }`)
		}
		this._addPropName?.(name)
		return this
	}
}
