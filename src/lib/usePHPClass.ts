import { useState, useEffect } from 'react'

const php = import('./createPhpClasses')

type State =
	| {
			value: string
			isPending: false
	  }
	| {
			value?: undefined
			isPending: true
	  }

export const usePHPClass = (code: string) => {
	const [s, set] = useState<State>({ isPending: true })
	useEffect(() => {
		let unmounted = false
		php.then(async ts => {
			if (unmounted) return
			const value = createValue(ts, code)
			if (s.isPending || s.value !== value)
				set(s => (s.value !== value ? { isPending: false, value } : s))
		})
		return () => {
			unmounted = true
		}
	}, [code, s, set])
	return s
}

const createValue = (ts: typeof import('./createPhpClasses'), code: string) => {
	const sourse = ts.createSource(code, 'tmp.ts')
	try {
		return ts
			.createPhpClasses(sourse)
			.map(file => file.lines.join('\n'))
			.join('\n\n')
	} catch (x) {
		console.error(x)
		return x?.code + ''
	}
}
