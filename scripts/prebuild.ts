import { remove } from 'fs-extra'
import { join } from 'path'

Promise.all([
	remove(join(__dirname, '..', 'out')),
	remove(join(__dirname, '..', 'public')),
]).catch(x => {
	console.error(x)
	process.exit(1)
})
